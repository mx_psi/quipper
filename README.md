# Quipper 

This is a copy of the [Quipper language v0.8](https://www.mathstat.dal.ca/~selinger/quipper/) with some minor modifications so that it is compatible with `stack` and has the QuipperLib libraries.
